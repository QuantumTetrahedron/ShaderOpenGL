#version 330 core
layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Color;
// Teraz każdy wierzchołek zawiera też dane o współrzędnych tekstury
layout(location = 2) in vec2 texCoords;

out vec3 color;
// Dane o teksturze nie są nam potrzebne w vertex shaderze więc przekazujemy je dalej
out vec2 TexCoords;

void main(){
    color = Color;
    TexCoords = texCoords;

    gl_Position = vec4(Position, 1.0f);
}