#version 330 core

out vec4 out_color;

in vec3 color;
// wczytujemy współrzędne tekstury podane z vertex shadera.
// na tym etapie zostały już przetworzone na ładny gradient tak jak działo się to z kolorem
// więc każdy piksel otrzyma inny punkt na teksturze. Tutaj punkt na teksturze to wciąż liczba rzeczywista a nie konkretny piksel tekstury.
in vec2 TexCoords;

// wczytujemy nasze utworzone tekstury
uniform sampler2D texture1;
uniform sampler2D texture2;

void main() {
    // pobieramy kolor z tekstury używając punktu zawierającego współrzędne
    // funkcja texture wyczyta na podstawie nich kolor z tekstury.
    // To nie musi być kolor zawierający się na teksturze.
    // Przyjmijmy przypadek wczytania tekstury 6x6 pikseli gdzie pierwsze 3 kolumny są białe a 3 kolejne czarne.
    // Chcemy tą teksturę nałożyć na obiekt kóry zajmie na ekranie 5x5 pikseli. Piksel znajdujący się na samym środku
    // otrzyma współrzędną tekstury (0.5, 0.5) - potem będziemy chcieli tą współrzędną przeliczyć na piksel obrazu 6x6.
    // Okaże się że współrzędna wylądowała na przecięciu siatki pikseli, więc 4 piksele są równie odległe od tego punktu.
    // Jeżeli przy tworzeniu obrazu ustawiliśmy opcję MIN_FILTER na GL_NEAREST to będzie to albo kolor biały albo czarny
    // (Prawdopodobnie zależny od dokładności obliczeń na floatach ale zawsze ten sam). Jeżeli wybraliśmy opcję
    // GL_LINEAR to piksel wynikowy będzie średnią tych 4 pikseli i dostaniemy kolor szary, którego nie było na oryginalnej teksturze.
    vec4 tex1 = texture( texture1, TexCoords );

    vec4 tex2 = texture( texture2, TexCoords );

    // Po wczytaniu konkretnej wartości piksela z obu obrazów można na nich wykonywać dowolne operacje
    // a następnie przypisać do koloru wyjściowego.
    out_color = tex1 * tex2;
    //out_color = tex1 + tex2;
    //out_color = tex1 / tex2;
    //out_color = abs(tex1 - tex2);
    //out_color = tex2 * color;
    //out_color = tex1 * tex2 * color;

    // Jeżeli obraz miał kanał alpha to można sugerować się jego wartością podczas obliczeń.
    // Należy wtedy pamiętać aby w main.cpp wczytywać przy wczytywaniu obrazu użyć formatu GL_RGBA zamiast GL_RGB.
    //if(tex2.a < 0.9)
    //    out_color = tex1;
    //else
    //    out_color = tex1*tex2;

    // albo w sumie poprawniej
    //out_color = mix( tex1, tex2, tex2.a);
}
