#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include <cmath>
#include "Shader.h"

/// Includujemy stb_image.h
/// Przed includem trzeba zdefiniować STB_IMAGE_IMPLEMENTATION, które modyfikuje plik tak aby zawierał
/// definicje funkcji. Wystarczy to zrobić raz w dowolnym pliku.
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

void ProcessInput(GLFWwindow* window);
void cursorPosCallback(GLFWwindow* window, double xPos, double yPos);

void FramebufferSizeCallback(GLFWwindow* window, int width, int height);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glfwSetCursorPosCallback(window, cursorPosCallback);
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    /// Rysujemy teraz 2 trójkąty tak aby stworzyły prostokąt na którym umieścimy teksturę.
    /// Dla każdego piksela w fragment shaderze potrzebujemy również współrzędnych z których pobierać dane.
    /// Podajemy te współrzędne dla każdego wierzchołka, a wartości wytworzą gradient w fragment shaderze tak jak działo się to z kolorem.
    /// Współrzędne tekstur zawierają się w przedziale <0,1> gdzie punkt (0,0) to lewy dolny róg.
    float vertices[] = {
            ///Pierwszy trójkąt.
            /// Position          Color              TexCoords
            -0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,  0.0f, 1.0f,    /// lewy górny wierzchołek - wsp : (0, 1)
            -0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,  0.0f, 0.0f,    /// lewy dolny wierzchołek - wsp : (0, 0)
             0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,  1.0f, 0.0f,    ///prawy dolny wierzchołek - wsp : (1, 0)

            ///Drugi trójkąt.
            /// Position          Color              TexCoords
            -0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,  0.0f, 1.0f,    /// lewy górny wierzchołek - wsp : (0, 1)
             0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,  1.0f, 0.0f,    ///prawy dolny wierzchołek - wsp : (1, 0)
             0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,  1.0f, 1.0f     ///prawy górny wierzchołek - wsp : (1, 1)
    };

    unsigned int VAO;
    unsigned int VBO;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE, 8*sizeof(float),(void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    /// Kolejne informacje w wierzchołku więc dodajemy kolejny atrybut.
    /// Tym razem 2 floaty zaczynające się po 6 pierwszych
    /// Trzeba też zmienić stride na 8*sizeof(float) we wcześniejszych atrybutach jako że rozmiar całego wierzchołka się powiększył.
    glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE, 8*sizeof(float),(void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    Shader shader("shaders/shader.vert", "shaders/shader.frag");

    /// Zanim wczytamy jakąś teksturę ustawiamy flagę stbi, która odwróci
    /// współrzędne y wczytanej tekstury tak aby była zgodna z przyjętą normą.
    stbi_set_flip_vertically_on_load(true);

    ////////////////////////////////////////////////////////////////////
    ///////////// wczytywanie pierwszej tekstury ///////////////////////
    ////////////////////////////////////////////////////////////////////

    /// wczytujemy dane z pliku zapisując informację o rozmiarze i ilości kanałów.
    int width, height, nrChannels;
    unsigned char* data = stbi_load("textures/wood.jpg", &width, &height, &nrChannels, 0);

    /// Tworzymy obiekt tekstury
    /// Procedura tworzenia podobna do tworzenia buforów i w sumie większości obiektów openGLowych.
    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    /// Ustawiamy parametry obrazu
    /// Co powinno dziać się kiedy współrzędne obrazu wyjdą poza zakres <0,1>
    /// Dostępne opcje to:
    ///     GL_REPEAT  -  Obraz będzie powtarzany
    ///     GL_MIRRORED_REPEAT - Obraz będzie się powtarzał odbijając się symetrycznie
    ///     GL_CLAMP_TO_EDGE - Wartości poza przedziałem dostaną kolor najbliższego punktu brzegowego obrazu.
    ///     GL_CLAMP_TO_BORDER - Wartości poza przedziałem otrzymają kolor przekazany przez funkcję glTexParameter4fv z parametrem GL_TEXTURE_BORDER_COLOR
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    /// Co powinno się dziać kiedy obraz musi zostać przeskalowany.
    /// Biorąc pod uwagę że tekstura, którą wczytujemy ma rozmiar 1500x1500 a wrzucamy ją na prostokąt o rozdzielczości 400x300 pikseli
    /// to można zakładać, że zostanie użyta mipmapa o rozmiarze 750x750 (kolejna - 375x375 - jest już za mała) a następnie
    /// zostanie przeskalowana do rozmiaru 400x300.
    /// Dostępne opcje to:
    ///     GL_LINEAR - Interpolacja liniowa. Kolor piksela zostanie wyznaczony na podstawie średniej ważonej pobliskich pikseli.
    ///     GL_NEAREST - Zostanie wybrany najbliższy dostępny piksel.
    ///     Różnica w skrócie jest taka, że jak przeskalujecie normalny obraz z opcją GL_LINEAR to powinien wyjść podobny
    ///               a z GL_NEAREST może wydawać się rozpikselowany. Natomiast skalując obraz przedstawiający szachownicę albo
    ///               grafikę, która z założenia ma być rozpikselizowana to lepiej użyć GL_NEAREST, żeby nie rozmazały się nam brzegi.
    /// Ustawiając ten parametr dla sytuacji, w której obraz jest mniejszy niż nasza tekstura, trzeba pamiętać że obraz może
    /// zostać podmieniony na inną teksturę o połowie rozmiaru wcześniejszej. Jako że mipmapy nie muszą być generowane
    /// automatycznie to może okazać się, że chcemy żeby mipmapy skalowały się inaczej (raczej nie będziemy chcieli ale kto nam zabroni).
    /// Specjalnie po to powstały opcje GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR oraz GL_NEAREST_MIPMAP_NEAREST.
    /// Te opcje mają sens tylko w przypadku skalowania w dół. Tzn. jeżeli wczytamy teksturę 16x16 pikseli to stworzy ona mipmapy o
    /// rozmiarach 8x8, 4x4, 2x2 oraz 1x1 a nie 32x32. Więc aplikując tą teksturę na obiekt zajmujący 400x400 pikseli wciąż będzie
    /// wykorzystywał oryginalną teksturę.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    /// Jeżeli obraz został poprawnie wczytany...
    if(data){
        /// ... przekazujemy te dane do naszego obiektu OpenGLowego.
        /// Parametry:
        /// target, level, internalformat, width, height, border, format, type, pixels
        /// target         - określa który obiekt wypełniamy danymi - ten zbindowany do GL_TEXTURE_2D
        /// level          - poziom mipmapy. Jeżeli nie chcemy polegać na automatycznym tworzeniu mipmap to możemy podawać swoje.
        ///                  Wtedy level określa która z kolei jest to mipmapa. Dla 0 generujemy zwykłą teksturę nie będącą mipmapą.
        /// internalformat - określa w jakim formacie zapisać dane w obiekcie tekstury. Najczęściej pokrywa się to z argumentem format.
        ///                  Istotne w przypadku gdy wczytujemy obraz z kanałem alpha a nie jest nam on potrzebny i nie chcemy zaśmiecać nim pamięci.
        /// width          - szerokość wczytanego obrazu
        /// height         - wysokość wczytanego obrazu
        /// border         - Ten argument musi być równy 0 bo tak
        /// format         - Format danych wejściowych. Internal format specyfikuje nasze preferencje natomiast format musi się zgadzać z
        ///                  faktycznym rodzajem pliku. Generalnie to można to określić odczytując ilość kanałów zwracaną przez stbi_load.
        ///                  3 kanały to GL_RGB, 4 to GL_RGBA a 1 to GL_RED.
        /// type           - typ danych wejściowych. stbi_load zwraca unsigned char* - char to w sumie liczba zapisana na bajcie, stąd GL_UNSIGNED_BYTE
        /// pixels         - wskaźnik na początek danych o pikselach
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

        /// Po wczytaniu obrazu generujemy automatycznie mipmapy.
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else std::cerr << "Failed to load image" << std::endl;
    glBindTexture(GL_TEXTURE_2D, 0);
    /// Pod koniec można zwolnić pamięć.
    /// stbi napisane jest w C i korzysta z takich wspaniałych funkcji jak malloc a stbi_image_free to "this is just free()"
    stbi_image_free(data);

    ////////////////////////////////////////////////////////////////////
    ///////////// wczytywanie drugiej tekstury /////////////////////////
    ////////////////////////////////////////////////////////////////////

    data = stbi_load("textures/lena.jpg", &width, &height, &nrChannels, 0);
    unsigned int texture2;
    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    if(data){
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else std::cerr << "Failed to load image" << std::endl;
    glBindTexture(GL_TEXTURE_2D, 0);
    stbi_image_free(data);
    ////////////////////////////////////////////////////////////////////

    /// W fragment shaderze używamy uniformów typu sampler2D
    /// do tych uniformów trzeba wysłać inta przypisującego do niego slot tekstury.
    shader.Use();
    shader.SetInt("texture1", 0); /// <-------------------------------
    shader.SetInt("texture2", 1); /// <-----------------------------//--------
                                                                    //      //
    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);                           //      //
    while (!glfwWindowShouldClose(window))                          //      //
    {                                                               //      //
        glClear(GL_COLOR_BUFFER_BIT);                               //      //
                                                                    //      //
        ProcessInput(window);                                       //      //
                                                                    //      //
        shader.Use();                                               //      //
        /// Do wybranych slotów przypisujemy tekstury               //      //
        glActiveTexture(GL_TEXTURE0); /// <---------------------------      //
        glBindTexture(GL_TEXTURE_2D, texture);                              //
        glActiveTexture(GL_TEXTURE1); /// <-----------------------------------
        glBindTexture(GL_TEXTURE_2D, texture2);

        glBindVertexArray(VAO);
        /// Teraz rysujemy 2 trójkąty więc 6 wierzchołków.
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void ProcessInput(GLFWwindow* window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void cursorPosCallback(GLFWwindow* window, double xPos, double yPos){
    //glClearColor(float(xPos / WIDTH), float(yPos / HEIGHT), 0.0f, 1.0f);
}

void FramebufferSizeCallback(GLFWwindow* window, int width, int height){
    glViewport(0,0,width, height);
}