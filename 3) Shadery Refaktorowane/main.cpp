#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

/// Chwilowo używamy cmath do funkcji trygonometrycznych ale jak zacznie się ciekawsza matematyka to znajdziemy coś lepszego.
#include <cmath>
#include "Shader.h"

void ProcessInput(GLFWwindow* window);
void cursorPosCallback(GLFWwindow* window, double xPos, double yPos);

/// Funkcja reagująca na zmianę rozmiaru okna.
void FramebufferSizeCallback(GLFWwindow* window, int width, int height);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glfwSetCursorPosCallback(window, cursorPosCallback);
    /// Rejestrowanie funkcji reagującej na zmianę rozmiaru okna.
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    /// Wierzchołki zawierają teraz również dane o swoim kolorze
    float vertices[] = {
            /// Position          Color
             0.0f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,
            -0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,
             0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f
    };

    unsigned int VAO;
    unsigned int VBO;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    /// Więcej danych o wierzchołkach więc bardziej lepiej trzeba je opisać
    /// Parametry glVertexAttribPointer dla przypomnienia:
    /// index, size, type, normalized, stride, offset
    /// index musi się zgadzać z 'location' w vertex shaderze, type to standardowo floaty.
    /// Dla reszty rysunek poglądowy:
    ///     Zawartość VBO:
    ///         --------------------------------------------------------
    ///         | x | y | z | r | g | b | x | y | z | r | g | b | x | ....
    ///         --------------------------------------------------------
    ///     Dane w VAO:
    ///   Attrib 0: (Position)
    ///   Size: <---------->                            - 3                 - ile wartości
    /// Stride: <---------------------->                - 6 * sizeof(float) - jaki jest odstęp pomiędzy tymi zmiennymi
    /// Offset: >                                       - 0 * sizeof(float) - gdzie znajduje się pierwsze wystąpienie
    ///   Attrib 1: (Color)
    ///   Size:             <---------->                - 3
    /// Stride:             <---------------------->    - 6 * sizeof(float)
    /// Offset: ----------->                            - 3 * sizeof(float)
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE, 6*sizeof(float),(void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    /// Nasz shader teraz jako fajna klasa.
    Shader shader("shaders/shader.vert", "shaders/shader.frag");

    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);
    while (!glfwWindowShouldClose(window))
    {
        glClear(GL_COLOR_BUFFER_BIT);

        ProcessInput(window);

        /// Zbieramy informację o aktualnym czasie i obliczamy gdzie przesunąć trójkąt.
        float x, y;
        y = (float)sin(glfwGetTime()) / 2.0f;
        x = (float)cos(glfwGetTime()) / 2.0f;

        /// Zanim wyślemy te dane do shadera trzeba zadeklarować że mamy zamiar go używać
        shader.Use();
        /// i wysyłamy używając metody klasy Shader.
        shader.SetVec2("PositionOffset", x, y);

        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void ProcessInput(GLFWwindow* window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void cursorPosCallback(GLFWwindow* window, double xPos, double yPos){
    //glClearColor(float(xPos / WIDTH), float(yPos / HEIGHT), 0.0f, 1.0f);
}

void FramebufferSizeCallback(GLFWwindow* window, int width, int height){
    /// Chcielibyśmy żeby nasza gra dostosowywała się do aktualnego rozmiaru okna i wyglądała zawsze tak samo.
    /// Ta funkcja tego nie zrobi.
    /// Tutaj określamy tylko w jakiej części ekranu rysujemy a chcemy zawsze rysować na całej powierzchni okna.
    /// Żeby zauważyć że nie działo się tak wcześniej wystarczy nie dzielić cosinusa przez 2 podczas obliczania przesunięcia.
    /// Trójkąt będzie wylatywał poza okno i jeżeli nie będziemy używać funkcji glViewport to po przeskalowaniu okna tak aby było ono szersze
    /// trójkąt wyleci poza zakres renderowania i widoczne będzie odcięcie:
    ///
    /// <----------------> Rozmiar oryginalny
    /// +--------------------------+
    /// |                          |
    /// |              /\          |
    /// |            /   |         |
    /// |          /     |         |
    /// |        /-------|         |
    /// +--------------------------+
    ///
    /// Po użyciu funkcji glViewport będziemy zawsze używać całego okna do renderowania
    /// natomiast zawsze będzie też odwzorowywało ono sławetny sześcian o boku długości 2 i środku masy w punkcie (0,0,0)
    /// Efekt jest taki że nasz trójkąt rozciąga się aby się dopasować.
    /// Czyli obraz nie jest taki sam na każdej rozdzielczości. Chwilowo jednak nie martwcie się tym aż tak. Będziemy to naprawiać już niedługo.
    glViewport(0,0,width, height);
    /// Ewentualnie można rozwiązać ten problem zakazując użytkownikowi skalowania okna.
    /// Można to osiągnąć odpowiednim hintem ustawionym przed stworzeniem okna.
    /// glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}