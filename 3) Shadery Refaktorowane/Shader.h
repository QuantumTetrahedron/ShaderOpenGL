#ifndef SHADER_H
#define SHADER_H

#include <string>

class Shader {
public:
    Shader(const char* vertexPath, const char* fragmentPath);

    void Use();

    void SetInt(std::string name, int value);
    void SetFloat(std::string name, float value);
    void SetBool(std::string name, bool value);
    void SetVec2(std::string name, float x, float y);
    void SetVec3(std::string name, float x, float y, float z);
    void SetVec4(std::string name, float x, float y, float z, float w);

private:
    unsigned int id;
    void LoadFromFile(const char* vertexPath, const char* fragmentPath);
};


#endif //SHADER_H
