
#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera {
public:
    Camera(glm::vec3 pos)
    : position(pos), pitch(0.0f), yaw(-90.0f), speed(3.0f), mouseSensitivity(0.5f), worldUp(0.0f,1.0f,0.0f){
        UpdateVectors();
    }

    glm::mat4 GetViewMatrix(){
        return glm::lookAt(position, position+front, up);
    }

    enum class Movement{
        forward, backward, left, right
    };

    void ProcessMovement(Movement direction, float dt){
        float velocity = dt * speed;
        if(direction == Movement::forward)
            position += front * velocity;
        else if(direction == Movement::backward)
            position -= front * velocity;
        else if(direction == Movement::right)
            position += right * velocity;
        else if(direction == Movement::left)
            position -= right * velocity;
    }

    void ProcessMouseMove(float xOffset, float yOffset){
        yaw += xOffset * mouseSensitivity;
        pitch += yOffset * mouseSensitivity;

        if(pitch > 89.0f)
            pitch = 89.0f;
        if(pitch < -89.0f)
            pitch = -89.0f;

        UpdateVectors();
    }

    glm::vec3 GetPosition(){
        return position;
    }
private:
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 worldUp;

    float speed;
    float mouseSensitivity;

    float pitch;
    float yaw;

    void UpdateVectors(){
        glm::vec3 Front;
        float p = glm::radians(pitch);
        float y = glm::radians(yaw);
        Front.x = glm::cos(p) * glm::cos(y);
        Front.y = glm::sin(p);
        Front.z = glm::cos(p) * glm::sin(y);
        front = glm::normalize(Front);

        right = glm::cross(front, worldUp);
        right = glm::normalize(right);

        up = glm::cross(right, front);
        up = glm::normalize(up);
    }
};

#endif
