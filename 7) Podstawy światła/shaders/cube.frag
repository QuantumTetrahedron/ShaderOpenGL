#version 330 core

out vec4 out_color;

// Wszystkie dane potrzebne do obliczeń światła
// Pozycja fragmentu
in vec3 FragPos;
// Wektor normalny
in vec3 normal;
// Kolor światła
uniform vec3 lightColor;
// Kolor obiektu
uniform vec3 objectColor;
// Pozycja światła
uniform vec3 lightPos;
// Pozycja kamery
uniform vec3 cameraPos;
// Dobrze, że zrobiłem te komentarze bo wcale to nie wynikało z nazw zmiennych xd
// (Autor nie chciał w ten sposób nikogo urazić insynuując brak podstawowej wiedzy z języka angielskiego. dop. red.)

void main(){
    /// Światło w modelu Phong jest sumą 3 składowych:
    /// Ambient - Mała ilość światła która zawsze wszędzie jest
    /// Diffuse - Najistotniejsza składowa - intensywność oświetlenia powierzchni zależna od kąta padania światłą
    /// Specular - Odbijające się światło trafia prosto w obiektyw (albo gdzieś w miarę blisko)

    /// Ambient to po prostu jakiś mały fragment światła.
    float ambient = 0.1;

    /// Diffuse to cosinus kąta pomiędzy wektorem normalnym a wektorem poprowadzonym do źródła światła
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    /// dot product zwraca cosinus dla znormalizowanych wektorów
    float diffuse = dot(norm, lightDir);
    /// światło nie powinno być ujemne
    diffuse = max(diffuse, 0);

    /// Specular to cosinus kąta pomiędzy wektorem światła odbitego a wektorem poprowadzonym do pozycji kamery podniesiony do jakiejś potęgi (która zwykle sama jest potęgą 2)
    // Zwykle jego wpływ jest silny więc zmniejszamy jego moc dla lepszych efektów
    float specularStrength = 0.5;
    // funkcja reflect odbija wektor względem danego wektora normalnego
    vec3 reflectDir = reflect(-lightDir, norm);
    reflectDir = normalize(reflectDir);
    vec3 cameraDir = normalize(cameraPos - FragPos);
    float specular = dot(reflectDir, cameraDir);
    // Też nie chcemy ujemnego światła
    specular = max(specular, 0);
    // Im wyższa potęga tym mniejszy ale też intensywniejszy jest obszar który zostaje w ten sposób rozświetlony
    specular = pow(specular, 32.0);
    specular = specular * specularStrength;

    /// Kolor światła mnożymy przez jego intensywność i nakładamy efekt na kolor obiektu, również mnożeniem.
    vec3 result = (ambient+diffuse+specular) * lightColor * objectColor;
    out_color = vec4(result, 1.0);
}