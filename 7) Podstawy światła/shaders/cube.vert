#version 330 core
layout (location = 0) in vec3 Position;
// Otrzymujemy teraz również wektor normalny - prostopadły do powierzchni
layout (location = 1) in vec3 Normal;

out vec3 FragPos;
// Wektor normalny przydatniejszy jest w fragment shaderze
out vec3 normal;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

void main(){
    // Obliczenia światła robimy w world space więc Normal też musi zostać skonwertowany do tej przestrzeni.
    // W przypadku gdy skala na osiach jest różna trzeba ją odwrócić aby wektor dalej był prostopadły.
    // inwersja macierzy odwraca skalę ale odwraca też obrót więc wykonujemy jeszcze dodatkowo transpozycję,
    // która ponownie odwróci obrót.
    normal = transpose(inverse(mat3(model))) * Normal;

    // W fragment shaderze potrzebna nem będzie też pozycja fragmentu w world space. Wystarczy pomnożyć przez macierz
    // model wierzchołki w vertex shaderze - ich pozycja w world space zostanie zgradientowana tak aby każdy fragment
    // otrzymał odpowiednią
    FragPos = vec3(model * vec4(Position, 1.0f));

    gl_Position = projection * view * model * vec4(Position, 1.0f);
}