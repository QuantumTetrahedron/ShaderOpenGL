
#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h>

class Texture {
public:
    Texture(const char* filePath);

    void Bind() const;

    int GetWidth() const;
    int GetHeight() const;
    int GetNumberOfChannels() const;
private:
    void LoadFromFile(const char* filePath);

    unsigned int id;
    int width, height, nrChannels;
};

#endif
