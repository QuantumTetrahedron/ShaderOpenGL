
/// GLAD odpowiada za przypisanie wskaźników do funkcji do odpowiednich nazw
///     Jest to potrzebne bo implementacja funkcji znajduje się na karcie graficznej
#include <glad/glad.h>

/// GLFW ułatwia wszystko związane z systemem operacyjnym
///     np. tworzenie okna czy zbieranie inputu.
/// Wykorzystuje define-y zawarte w glad.h więc musi być zaincludowany po nim.
#include <GLFW/glfw3.h>

#include <iostream>

/// deklaracje funkcji zbierających input
/// 1.) Zwykła funkcja która będzie wywoływana wewnątrz głównej pętli programu
void ProcessInput(GLFWwindow* window);

/// 2.) Callback przypisany do konkretnego zdarzenia wywoływany w ramach funkcji glfwPollEvents()
void cursorPosCallback(GLFWwindow* window, double xPos, double yPos);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

int main()
{
    /// Inicjalizacja GLFW
    glfwInit();
    /// Wybieramy wersję 3.3 Core
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /// Dodatkowy hint który podobno potrzebny jest dla Os X (nie mam jak przetestować czy to faktycznie coś zmienia)
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    /// Tworzenie okna - glfwCreateWindow
    /** Parametry:
    * width     -   szerokość
    * height    -   wysokość
    * title     -   tytuł
    * monitor   -   Jeżeli wybierzemy monitor to gra włączy się w pełnym ekranie
                    w takich sytuacjach dobrze mieć wysokość i szerokość ekranu
    * share     -   Wskaźnik na parent window gdybyśmy potrzebowali kilku.
    */
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
    /// Poprawny FullScreen:
    /*
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* videoMode = glfwGetVideoMode(monitor);
    GLFWwindow* window = glfwCreateWindow(videoMode->width, videoMode->height, "ShaderOpenGL", monitor, nullptr);
    */
    /// Sprawdzamy czy okno zostało utworzone poprawnie
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    /// Ustawianie stworzonego okna jako aktywny kontekst
    glfwMakeContextCurrent(window);

    /// Inicjalizacja GLADa
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    /// Przypisywanie funkcji reagujacej na zdarzenia
    glfwSetCursorPosCallback(window, cursorPosCallback);


    /// Wybieranie koloru tła wykorzystywanego przez glClear
    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);

    /// main loop
    while (!glfwWindowShouldClose(window))
    {
        /// Czyszczenie ekranu wybranym kolorem
        glClear(GL_COLOR_BUFFER_BIT);

        ProcessInput(window);

        /// Podmienienie przedniego i tylnego bufora
        /// Bez tego nic nie pojawi się na ekranie jako że wszystkie funkcje rysujące (w tym glClear)
        /// updateuja buffer który nie jest aktualnie wyświetlany.
        glfwSwapBuffers(window);
        /// Przechwytywanie zdarzeń takich jak wciśnięcie przycisku lub ruch kursora
        glfwPollEvents();
    }

    /// Zwolnienie pamięci zaalokowanej przez GLFW
    glfwTerminate();
    return 0;
}

/// Zamiast tej funkcji można zarejestrować callback reagujący na wciśnięcie klawisza (glfwSetKeyCallback)
void ProcessInput(GLFWwindow* window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

void cursorPosCallback(GLFWwindow* window, double xPos, double yPos){
    glClearColor(float(xPos / WIDTH), float(yPos / HEIGHT), 0.0f, 1.0f);
    /// Tu oczywiście jeżeli przeskalujemy okno podczas działania programu to nie będzie działać poprawnie
    /// Jeżeli po podzieleniu dostaniemy wartość > 1.0 to zostanie automatycznie zmieniona na 1.0.
    /// Można ewentualnie zarejestrować callback na event zmiany rozmiaru okna używając glfwSetWindowSizeCallback()
    /// i updateować zmienne WIDTH i HEIGHT (nie muszą być const)
    /// Innym rozwiązaniem jest stworzenie okna którego nie da się przeskalować
    /// używając glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}
