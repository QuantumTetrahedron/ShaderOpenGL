#version 330 core

out vec4 out_color;

/// Otrzymujemy kolor z vertex shadera
in vec3 color;

/// Tutaj też mógłby być uniform i też wymagałby tylko użycia funkcji glUniformNX.
/// Przykładowo:
/// uniform float intensity;
/// intensywność mogłaby być ustawiana jako i = (cos(glfwGetTime()) + 1.0f) / 2.0f
/// i wysyłana funkcją glUniform1f(glGetUniformLocation(shader, "intensity"), i);

void main() {
    /// color = color * intensity;

    out_color = vec4(color, 1.0f);
}
