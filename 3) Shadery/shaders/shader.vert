#version 330 core
/// Teraz każdy wierzchołek ma inny kolor więc otrzymujemy go używając zmiennej wejściowej (in)
layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Color;

/// Chcemy przekazać kolor do fragment shadera bo dopiero tam może być odpowiednio wykorzystany
/// Ale co? Ale jak to tak? - Zastanawiacie się pewnie - Vertex shader wykona się 3 razy za każdym razem z innym
/// kolorem a fragment shader wykonuje się tyle razy ile trójkąt zajmie pikseli!
/// Owszem wyjściowy trójkąt zajmie pewnie więcej niż 3 piksele, natomiast wartość każdej przesłanej w ten sposób
/// zmiennej zostaje liniowo zinterpolowana, więc każdy piksel otrzyma odpowiednią kombinację danych
/// z wierzchołków trójkąta, w którym się znajduje.
out vec3 color;

/// Zmienna uniform to zmienna, której wartość możemy zmienić w dowolnym momencie podczas działania programu.
/// Jest globalna więc ma tą samą wartość dla każdego wierzchołka (i dla każdego piksela jeżeli znajduje się w fragment shaderze).
/// Wystarczy wysłać ją raz a zachowa swoją wartość dopóki nie otrzyma nowej.
/// Zmienna typu uniform nie jest niestety globalna na przestrzeni kilku shaderów.
/// Jeżeli mielibyśmy jeden shader do rysowania trójkątów zielonych i jeden do trójkątów czerwonych
/// a każdy zawierałby w sobie zmienną vec2 PositionOffset, to trzeba byłoby
/// użyć w kodzie 2 razy funkcji glUniform2f używając za każdym razem innego shadera.
uniform vec2 PositionOffset;

void main(){
    /// Kolor zwyczajnie przekazujemy dalej
    color = Color;

    /// Pozycję wierzchołka obliczamy dodając PositionOffset do składowych x i y wejściowej pozycji wierzchołka.
    gl_Position = vec4(Position.xy + PositionOffset, Position.z, 1.0f);
}