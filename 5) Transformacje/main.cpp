#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "Shader.h"
#include "Texture.h"

/// inludujemy glm żeby mieć dostęp do wektorów i macierzy
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

void ProcessInput(GLFWwindow* window);
void cursorPosCallback(GLFWwindow* window, double xPos, double yPos);

void FramebufferSizeCallback(GLFWwindow* window, int width, int height);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glfwSetCursorPosCallback(window, cursorPosCallback);
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    /// Depth test sprawia że po wyjściu z fragment shadera testowana jest odległość od kamery dla tego fragmentu.
    /// Jeżeli odległość fragmentu od kamery jest mniejsza niż aktualna wartość depth buffera to depth buffer zostaje zaktualizowany a fragment narysowany.
    glEnable(GL_DEPTH_TEST);

    /// Wierzchołki sześcianu -- tym razem bez koloru więc trzeba też zmienić atrybuty
    float vertices[] = {
             /// position         texCoord
            /// Tył
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
            /// Przód
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            /// Lewa strona
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            /// Prawa strona
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            /// Dół
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            /// Góra
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };

    unsigned int VAO;
    unsigned int VBO;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    /// Pierwsze 3 wartości to pozycja
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    /// Kolejne 2 to wsp. tekstury               łącznie 5 floatów
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE, 5*sizeof(float),(void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


    Shader shader("shaders/shader.vert", "shaders/shader.frag");

    Texture texture1("textures/wood.jpg");
    Texture texture2("textures/rabbit.png");

    shader.Use();
    shader.SetInt("texture1", 0);
    shader.SetInt("texture2", 1);

    /// Macierz view konwertująca world space na view space.
    /// Więcej o view następnym razem - chwilowo prowizoryczna kamera statyczna w punkcie (0,0,3)
    /// Kamera w view space zawsze patrzy w stronę -z, czyli w głąb ekranu.
    glm::mat4 view(1.0f);
    view = glm::translate(view, glm::vec3(0.0f,0.0f, -3.0f));
    shader.SetMat4("view", view);

    /// Macierz projection konwertująca przestrzeń w taki sposób że każda współrzędna każdego punktu w
    /// docelowym obszarze widzianym ląduje w przedziale <-1,1>
    /// Konwertuje view space na clip space.
    glm::mat4 projection=glm::perspective(glm::radians(45.0f), (float)WIDTH/HEIGHT,0.1f, 100.0f);
    shader.SetMat4("projection", projection);

    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);
    while (!glfwWindowShouldClose(window))
    {
        /// Trzeba czyścić też depth buffer, żeby testy nie brały pod uwagę stanu z poprzedniej klatki.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ProcessInput(window);

        shader.Use();

        /// Macierz model określająca położenie, obrót i skalę obiektów na scenie.
        /// Konwertuje local space na world space.
        glm::mat4 model(1.0f);
        /// model matrix jest najczęściej złożeniem 3 macierzy w kolejności translate * rotate * scale
        /// W kodzie macierze podawane są w tej samej kolejności jak w równaniu natomiast wykonują się odwrotnie bo:
        /// (translate * rotate * scale) * v = ( translate * ( rotate * ( scale * v ) ) )
        /// No i oczywiście mnożenie macierzy nie jest przemienne.
        //model = glm::translate(model, glm::vec3(0.5f, -0.5f, 0.0f));
        model = glm::rotate(model, (float)glfwGetTime(), glm::vec3(1.0f, 0.5f, 0.0f));
        model = glm::scale(model, glm::vec3(0.5f,0.5f,0.5f));

        shader.SetMat4("model", model);

        glActiveTexture(GL_TEXTURE0);
        texture1.Bind();
        glActiveTexture(GL_TEXTURE1);
        texture2.Bind();

        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        /// Ustawiamy inną macierz model i rysujemy sześcian jeszcze raz - tym razem w innym miejscu

        /// Tym razem translate wykona się przed rotate więc sześcian będzie obracał się po orbicie względem punktu (0,0,0)
        /// Oczywiście nic nie stoi na przeszkodzie żeby użyć jednego rodzaju operacji 2 razy, tzn
        /// Dodając 2 raz translate, tym razem przed rotate, to sześcian nalpierw się przesunie, potem obróci a potem znów przesunie.
        /// Da to efekt orbitowania względem punktu podanego przy pierwszej translacji w kodzie.
        model = glm::mat4(1.0f);
        /// model = glm::translate(model, glm::vec3(0.0f,1.0f,0.0f)); // sześcian będzie na orbicie punktu (0,1,0).
        model = glm::rotate(model, (float)glfwGetTime(), glm::vec3(0.0f,1.0f,0.0f));
        model = glm::translate(model, glm::vec3(1.0f, 0.0f,0.0f));
        model = glm::scale(model, glm::vec3(0.2f, 1.0f,1.0f));
        shader.SetMat4("model", model);

        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void ProcessInput(GLFWwindow* window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void cursorPosCallback(GLFWwindow* window, double xPos, double yPos){
    //glClearColor(float(xPos / WIDTH), float(yPos / HEIGHT), 0.0f, 1.0f);
}

void FramebufferSizeCallback(GLFWwindow* window, int width, int height){
    glViewport(0,0,width, height);
}