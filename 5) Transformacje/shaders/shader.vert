#version 330 core
// Tym razem w layoucie nie ma koloru
layout(location = 0) in vec3 Position;
layout(location = 1) in vec2 texCoords;

out vec2 TexCoords;

// Dostajemy potrzebne macierze z zewnątrz.
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(){
    TexCoords = texCoords;

    // Mnożymy odpowiednio nasze macierze
    // Można też przesłać od razu pomnożoną P*V*M, żeby nie mnożyć tego samego licząc wierzchołki
    // ale przy liczeniu świateł przyda nam się osobno sam model.
    // Często mnożenie jest wykonywane wcześniej a przesyłane są macierze:
    // model, view, projection, modelView, viewProjection i modelViewProjection -- tzn przesyłane są tylko te które są potrzebne :P
    // Tylko po to żeby mieć dostęp do wszystkich kombinacji nie wykonując drogiego mnożenia w vertex shaderze.
    gl_Position = projection * view * model * vec4(Position, 1.0f);
}