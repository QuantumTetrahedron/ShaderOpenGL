#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include <cmath>
#include "Shader.h"
#include "Texture.h"


void ProcessInput(GLFWwindow* window);
void cursorPosCallback(GLFWwindow* window, double xPos, double yPos);

void FramebufferSizeCallback(GLFWwindow* window, int width, int height);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glfwSetCursorPosCallback(window, cursorPosCallback);
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    /// Rysujemy teraz 2 trójkąty tak aby stworzyły prostokąt na którym umieścimy teksturę.
    /// Dla każdego piksela w fragment shaderze potrzebujemy również współrzędnych z których pobierać dane.
    /// Podajemy te współrzędne dla każdego wierzchołka, a wartości wytworzą gradient w fragment shaderze tak jak działo się to z kolorem.
    /// Współrzędne tekstur zawierają się w przedziale <0,1> gdzie punkt (0,0) to lewy dolny róg.
    float vertices[] = {
            ///Pierwszy trójkąt.
            /// Position          Color              TexCoords
            -0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,  0.0f, 1.0f,    /// lewy górny wierzchołek - wsp : (0, 1)
            -0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,  0.0f, 0.0f,    /// lewy dolny wierzchołek - wsp : (0, 0)
             0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,  1.0f, 0.0f,    ///prawy dolny wierzchołek - wsp : (1, 0)

            ///Drugi trójkąt.
            /// Position          Color              TexCoords
            -0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,  0.0f, 1.0f,    /// lewy górny wierzchołek - wsp : (0, 1)
             0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,  1.0f, 0.0f,    ///prawy dolny wierzchołek - wsp : (1, 0)
             0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,  1.0f, 1.0f     ///prawy górny wierzchołek - wsp : (1, 1)
    };

    unsigned int VAO;
    unsigned int VBO;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 8*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE, 8*sizeof(float),(void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    /// Kolejne informacje w wierzchołku więc dodajemy kolejny atrybut.
    /// Tym razem 2 floaty zaczynające się po 6 pierwszych
    /// Trzeba też zmienić stride na 8*sizeof(float) we wcześniejszych atrybutach jako że rozmiar całego wierzchołka się powiększył.
    glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE, 8*sizeof(float),(void*)(6*sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    Shader shader("shaders/shader.vert", "shaders/shader.frag");


    /// wczytywanie pierwszej tekstury
    Texture texture1("textures/wood.jpg");

    /// wczytywanie drugiej tekstury
    Texture texture2("textures/lena.jpg");

    /// W fragment shaderze używamy uniformów typu sampler2D
    /// do tych uniformów trzeba wysłać inta przypisującego do niego slot tekstury.
    shader.Use();
    shader.SetInt("texture1", 0); /// <-------------------------------
    shader.SetInt("texture2", 1); /// <-----------------------------//--------
                                                                    //      //
    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);                           //      //
    while (!glfwWindowShouldClose(window))                          //      //
    {                                                               //      //
        glClear(GL_COLOR_BUFFER_BIT);                               //      //
                                                                    //      //
        ProcessInput(window);                                       //      //
                                                                    //      //
        shader.Use();                                               //      //
        /// Do wybranych slotów przypisujemy tekstury               //      //
        glActiveTexture(GL_TEXTURE0); /// <---------------------------      //
        texture1.Bind();                                                    //
        glActiveTexture(GL_TEXTURE1); /// <-----------------------------------
        texture2.Bind();

        glBindVertexArray(VAO);
        /// Teraz rysujemy 2 trójkąty więc 6 wierzchołków.
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void ProcessInput(GLFWwindow* window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void cursorPosCallback(GLFWwindow* window, double xPos, double yPos){
    //glClearColor(float(xPos / WIDTH), float(yPos / HEIGHT), 0.0f, 1.0f);
}

void FramebufferSizeCallback(GLFWwindow* window, int width, int height){
    glViewport(0,0,width, height);
}