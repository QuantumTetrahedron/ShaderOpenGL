
#include <iostream>
#include "Texture.h"

/// Includujemy stb_image.h
/// Przed includem trzeba zdefiniować STB_IMAGE_IMPLEMENTATION, które modyfikuje plik tak aby zawierał
/// definicje funkcji. Wystarczy to zrobić raz w dowolnym pliku.
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

Texture::Texture(const char *filePath) {
    LoadFromFile(filePath);
}

void Texture::Bind() const {
    glBindTexture(GL_TEXTURE_2D, id);
}

void Texture::LoadFromFile(const char *filePath) {
    /// Zanim wczytamy jakąś teksturę ustawiamy flagę stbi, która odwróci
    /// współrzędne y wczytanej tekstury tak aby była zgodna z przyjętą normą.
    stbi_set_flip_vertically_on_load(true);

    /// wczytujemy dane z pliku zapisując informację o rozmiarze i ilości kanałów.
    unsigned char* data = stbi_load(filePath, &width, &height, &nrChannels, 0);

    /// Tworzymy obiekt tekstury
    /// Procedura tworzenia podobna do tworzenia buforów i w sumie większości obiektów openGLowych.
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    /// Ustawiamy parametry obrazu
    /// Co powinno dziać się kiedy współrzędne obrazu wyjdą poza zakres <0,1>
    /// Dostępne opcje to:
    ///     GL_REPEAT  -  Obraz będzie powtarzany
    ///     GL_MIRRORED_REPEAT - Obraz będzie się powtarzał odbijając się symetrycznie
    ///     GL_CLAMP_TO_EDGE - Wartości poza przedziałem dostaną kolor najbliższego punktu brzegowego obrazu.
    ///     GL_CLAMP_TO_BORDER - Wartości poza przedziałem otrzymają kolor przekazany przez funkcję glTexParameter4fv z parametrem GL_TEXTURE_BORDER_COLOR
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    /// Co powinno się dziać kiedy obraz musi zostać przeskalowany.
    /// Biorąc pod uwagę że tekstura, którą wczytujemy ma rozmiar 1500x1500 a wrzucamy ją na prostokąt o rozdzielczości 400x300 pikseli
    /// to można zakładać, że zostanie użyta mipmapa o rozmiarze 750x750 (kolejna - 375x375 - jest już za mała) a następnie
    /// zostanie przeskalowana do rozmiaru 400x300.
    /// Dostępne opcje to:
    ///     GL_LINEAR - Interpolacja liniowa. Kolor piksela zostanie wyznaczony na podstawie średniej ważonej pobliskich pikseli.
    ///     GL_NEAREST - Zostanie wybrany najbliższy dostępny piksel.
    ///     Różnica w skrócie jest taka, że jak przeskalujecie normalny obraz z opcją GL_LINEAR to powinien wyjść podobny
    ///               a z GL_NEAREST może wydawać się rozpikselowany. Natomiast skalując obraz przedstawiający szachownicę albo
    ///               grafikę, która z założenia ma być rozpikselizowana to lepiej użyć GL_NEAREST, żeby nie rozmazały się nam brzegi.
    /// Ustawiając ten parametr dla sytuacji, w której obraz jest mniejszy niż nasza tekstura, trzeba pamiętać że obraz może
    /// zostać podmieniony na inną teksturę o połowie rozmiaru wcześniejszej. Jako że mipmapy nie muszą być generowane
    /// automatycznie to może okazać się, że chcemy żeby mipmapy skalowały się inaczej (raczej nie będziemy chcieli ale kto nam zabroni).
    /// Specjalnie po to powstały opcje GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR oraz GL_NEAREST_MIPMAP_NEAREST.
    /// Te opcje mają sens tylko w przypadku skalowania w dół. Tzn. jeżeli wczytamy teksturę 16x16 pikseli to stworzy ona mipmapy o
    /// rozmiarach 8x8, 4x4, 2x2 oraz 1x1 a nie 32x32. Więc aplikując tą teksturę na obiekt zajmujący 400x400 pikseli wciąż będzie
    /// wykorzystywał oryginalną teksturę.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    /// Jeżeli obraz został poprawnie wczytany...
    if(data){
        /// ... przekazujemy te dane do naszego obiektu OpenGLowego.
        /// Parametry:
        /// target, level, internalformat, width, height, border, format, type, pixels
        /// target         - określa który obiekt wypełniamy danymi - ten zbindowany do GL_TEXTURE_2D
        /// level          - poziom mipmapy. Jeżeli nie chcemy polegać na automatycznym tworzeniu mipmap to możemy podawać swoje.
        ///                  Wtedy level określa która z kolei jest to mipmapa. Dla 0 generujemy zwykłą teksturę nie będącą mipmapą.
        /// internalformat - określa w jakim formacie zapisać dane w obiekcie tekstury. Najczęściej pokrywa się to z argumentem format.
        ///                  Istotne w przypadku gdy wczytujemy obraz z kanałem alpha a nie jest nam on potrzebny i nie chcemy zaśmiecać nim pamięci.
        /// width          - szerokość wczytanego obrazu
        /// height         - wysokość wczytanego obrazu
        /// border         - Ten argument musi być równy 0 bo tak
        /// format         - Format danych wejściowych. Internal format specyfikuje nasze preferencje natomiast format musi się zgadzać z
        ///                  faktycznym rodzajem pliku. Generalnie to można to określić odczytując ilość kanałów zwracaną przez stbi_load.
        ///                  3 kanały to GL_RGB, 4 to GL_RGBA a 1 to GL_RED.
        /// type           - typ danych wejściowych. stbi_load zwraca unsigned char* - char to w sumie liczba zapisana na bajcie, stąd GL_UNSIGNED_BYTE
        /// pixels         - wskaźnik na początek danych o pikselach
        if(nrChannels == 1)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, data);
        else if(nrChannels == 2)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG, width, height, 0, GL_RG, GL_UNSIGNED_BYTE, data);
        else if(nrChannels == 3)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        else if(nrChannels == 4)
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

        /// Po wczytaniu obrazu generujemy automatycznie mipmapy.
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else std::cerr << "Failed to load image" << std::endl;
    glBindTexture(GL_TEXTURE_2D, 0);
    /// Pod koniec można zwolnić pamięć.
    /// stbi napisane jest w C i korzysta z takich wspaniałych funkcji jak malloc a stbi_image_free to "this is just free()"
    stbi_image_free(data);
}

int Texture::GetWidth() const {
    return width;
}

int Texture::GetHeight() const {
    return height;
}

int Texture::GetNumberOfChannels() const {
    return nrChannels;
}
