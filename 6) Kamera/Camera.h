
#ifndef KURSOPENGL6_CAMERA_H
#define KURSOPENGL6_CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Camera {
public:

    /// Konstruktor ustawiający wartości początkowe
    Camera(glm::vec3 pos)
    : position(pos), pitch(0.0f), yaw(-90.0f), speed(3.0f), mouseSensitivity(0.5f), worldUp(0.0f,1.0f,0.0f){
        UpdateVectors();
    }

    /// Metoda zwracająca view.
    glm::mat4 GetViewMatrix(){
        /// glm sam tworzy macierz o postaci tej ze slajdów używając funkcji lookAt
        /// Parametry
        /// eye - pozycja kamery
        /// target - miejsce w które patrzy kamera
        /// up - góra względem kamery
        return glm::lookAt(position, position+front, up);
    }

    enum class Movement{
        forward, backward, left, right
    };

    /// Logika ruchu
    /// Zależnie od kierunku dodajemy lub odejmujemy front lub right
    /// Wszystko przemnożone przez delta time i jakąś zmienną określającą aktualną szybkość.
    void ProcessMovement(Movement direction, float dt){
        float velocity = dt * speed;
        if(direction == Movement::forward)
            position += front * velocity;
        else if(direction == Movement::backward)
            position -= front * velocity;
        else if(direction == Movement::right)
            position += right * velocity;
        else if(direction == Movement::left)
            position -= right * velocity;
    }

    /// Logika ruchu myszką
    /// Odpowiednio zwiększamy lub zmniejszamy kąty, ograniczamy pitch, żeby nie robić dziwnych akrobacji w powietrzu.
    /// Pod koniec aktualizujemy wektory kamery.
    void ProcessMouseMove(float xOffset, float yOffset){
        yaw += xOffset * mouseSensitivity;
        pitch += yOffset * mouseSensitivity;

        if(pitch > 89.0f)
            pitch = 89.0f;
        if(pitch < -89.0f)
            pitch = -89.0f;

        UpdateVectors();
    }
private:
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 worldUp;

    float speed;
    float mouseSensitivity;

    float pitch;
    float yaw;

    /// Przy każdej zmianie kątów trzeba zaktualizować wektory kamery
    void UpdateVectors(){
        /// front łatwo obliczyć na podstawie kątów -- pomocnicze obrazki są na prezentacji
        glm::vec3 Front;
        float p = glm::radians(pitch);
        float y = glm::radians(yaw);
        Front.x = glm::cos(p) * glm::cos(y);
        Front.y = glm::sin(p);
        Front.z = glm::cos(p) * glm::sin(y);
        /// Pod koniec wypadałoby znormalizować wektor
        front = glm::normalize(Front);

        /// kolejne wektory obliczamu używając cros productu.
        /// Do samej macierzy potrzebny nam już tylko up ale mamy tylko front i worldUp a z nich wyciągniemy tylko right,
        /// który i tak wykorzystujemy przy przesuwaniu kamery.
        right = glm::cross(front, worldUp);
        right = glm::normalize(right);

        /// Jak już mamy right to za jego pomocą obliczamy up.
        /// Obliczając wszystko w ten sposób mamy pewność że pomiędzy każdą parą wektorów front, right i up jest kąt prosty.
        up = glm::cross(right, front);
        up = glm::normalize(up);
    }
};


#endif //KURSOPENGL6_CAMERA_H
