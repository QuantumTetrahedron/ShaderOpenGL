#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <glm/glm.hpp>

class Shader {
public:
    Shader(const char* vertexPath, const char* fragmentPath);

    void Use();

    void SetInt(std::string name, int value);
    void SetFloat(std::string name, float value);
    void SetBool(std::string name, bool value);

    void SetVec2(std::string name, float x, float y);
    void SetVec2(std::string name, glm::vec2 v);

    void SetVec3(std::string name, float x, float y, float z);
    void SetVec3(std::string name, glm::vec3 v);

    void SetVec4(std::string name, float x, float y, float z, float w);
    void SetVec4(std::string name, glm::vec4 v);

    void SetMat4(std::string name, glm::mat4 matrix);

private:
    unsigned int id;
    void LoadFromFile(const char* vertexPath, const char* fragmentPath);
};


#endif //SHADER_H
