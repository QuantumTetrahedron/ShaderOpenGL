#version 330 core

out vec4 out_color;

in vec2 TexCoords;

uniform sampler2D texture1;
uniform sampler2D texture2;

void main() {
    vec4 tex1 = texture( texture1, TexCoords );
    vec4 tex2 = texture( texture2, TexCoords );

    out_color = mix(tex1, tex2, tex2.a);
}
