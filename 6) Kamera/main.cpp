#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>

#include "Shader.h"
#include "Texture.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Camera.h"

void ProcessInput(GLFWwindow* window);
void cursorPosCallback(GLFWwindow* window, double xPos, double yPos);

void FramebufferSizeCallback(GLFWwindow* window, int width, int height);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

/// Kamera stworzona globalnie żeby móc się do niej odnosić z callbacków.
/// Da się to zrobić ładniej np z pomocą lambd.
Camera camera(glm::vec3(0.0f,0.0f,5.0f));

/// Delta time - czas potrzebny na narysowanie ostatniej klatki
float deltaTime = 0.0f;
/// zmienna pomocnicza do obliczania deltaTime
float lastTime = 0.0f;

/// Pozycja kursora w ostatniej klatce
float lastX, lastY;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    /// Ponownie używamy callbacku dla pozycji kursora aby obracać kamerę
    glfwSetCursorPosCallback(window, cursorPosCallback);
    /// Tym razem z wyłączonym kursorem aby nie wylatywał za okno przy obracaniu
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glEnable(GL_DEPTH_TEST);

    float vertices[] = {
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
    };

    unsigned int VAO;
    unsigned int VBO;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE, 5*sizeof(float),(void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    Shader shader("shaders/shader.vert", "shaders/shader.frag");

    Texture texture1("textures/wood.jpg");
    Texture texture2("textures/rabbit.png");

    shader.Use();
    shader.SetInt("texture1", 0);
    shader.SetInt("texture2", 1);

    glm::mat4 projection=glm::perspective(glm::radians(45.0f), (float)WIDTH/HEIGHT,0.1f, 100.0f);
    shader.SetMat4("projection", projection);

    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);
    while (!glfwWindowShouldClose(window))
    {
        /// Obliczanie deltaTime
        float currentTime = (float)glfwGetTime();
        deltaTime = currentTime - lastTime;
        lastTime = currentTime;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ProcessInput(window);

        shader.Use();

        /// Pobieramy view z kamery i wysyłamy do shadera
        glm::mat4 view = camera.GetViewMatrix();
        shader.SetMat4("view", view);

        /// Rysujemy pare sześcianów w pętli żeby działo się coś więcej na scenie
        for(int i = -2; i <= 2 ; ++i) {
            glm::mat4 model(1.0f);
            model = glm::translate(model, glm::vec3(i, 0.0f,0.0f));
            model = glm::rotate(model, (float) glfwGetTime() * i, glm::vec3(1.0f, 0.5f, 0.0f));
            model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));

            shader.SetMat4("model", model);

            glActiveTexture(GL_TEXTURE0);
            texture1.Bind();
            glActiveTexture(GL_TEXTURE1);
            texture2.Bind();

            glBindVertexArray(VAO);
            glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);

            glBindTexture(GL_TEXTURE_2D, 0);
        }
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void ProcessInput(GLFWwindow* window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /// Obsługa WSADu - przekazujemy też delta time do kamery aby ruch był tak samo szybki nie zależnie od sprzętu
    if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessMovement(Camera::Movement::forward, deltaTime);
    if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessMovement(Camera::Movement::backward, deltaTime);
    if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessMovement(Camera::Movement::left, deltaTime);
    if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessMovement(Camera::Movement::right, deltaTime);
}

void cursorPosCallback(GLFWwindow* window, double xPos, double yPos){
    /// Przy pierwszym wywołaniu callbacka ustawiamy początkowe wartości pozycji kursora.
    lastX = xPos;
    lastY = yPos;

    /// Potem przestawiamy ten callback na inny - tym razem podany jako lambda.
    /// W ten sposób ten pierwszy callback wywoła się tylko raz.
    /// Innym rozwiązaniem byłby jakiś if który sprawdza czy te wartości są już ustalone albo czy jest to pierwsza klatka programu
    /// ale przypisując inny callback mamy jednego ifa mniej w każdej klatce.
    glfwSetCursorPosCallback(window, [](GLFWwindow* window, double xPos, double yPos){
        /// Obliczamy o ile przesunął się kursor względem ostatniej klatki
        float xOffset = xPos - lastX;
        float yOffset = lastY - yPos;
        /// Aktualizujemy wartości
        lastX = xPos;
        lastY = yPos;
        /// Przekazujemy offset do kamery.
        camera.ProcessMouseMove(xOffset, yOffset);
    });
}

void FramebufferSizeCallback(GLFWwindow* window, int width, int height){
    glViewport(0,0,width, height);
}