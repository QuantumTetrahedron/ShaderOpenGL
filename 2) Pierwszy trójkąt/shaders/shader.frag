/*
* GLSL - openGL Shading Language
* fragment shader
* odpalany dla każdego piksela zajmowanego przez renderowany obiekt
* określa wyjściowy kolor danego piksela
*/

// wersja powinna być zgodna z wersją openGLa 3.3->330 4.5->450 itp
#version 330 core

// musi być zadeklarowana jedna zmienna wyjściowa typu vec4 o dowolnej nazwie
out vec4 color;

void main() {
    // Żeby nie komplikować to każdy piksel otrzyma ten sam kolor
    color = vec4(1.0f,0.0f,0.0f,1.0f);
}
