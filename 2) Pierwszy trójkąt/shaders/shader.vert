/*
* GLSL - openGL Shading Language
* vertex shader
* odpalany dla każdego wierzchołka
* określa wyjściową pozycję wierzchołka
*/

// wersja powinna być zgodna z wersją openGLa 3.3->330 4.5->450 itp
#version 330 core
// przyjmowanie danych z VAO
// location takie jak indeks atrybutu a typ musi się zgadzać z ilością oraz typem danych wysłanych
layout(location = 0) in vec3 Position;
// vec2 vec3 i vec4 akceptują floaty,
//bvec* - boole
//dvec* - double
//ivec* - inty
//uvec* - unsigned inty

void main(){
    // efektem pracy vertex shadera musi być przypisanie wartości do zmiennej wbudowanej vec4 gl_Position
    gl_Position = vec4(Position, 1.0f);
}