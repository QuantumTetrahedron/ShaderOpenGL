#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

/// Funkcja ładująca shader z pliku (przeniesiemy tą funkcjonalność do klasy Shader na kolejnych zajęciach)
unsigned int LoadShader(const char* vertPath, const char* fragPath);

void ProcessInput(GLFWwindow* window);
void cursorPosCallback(GLFWwindow* window, double xPos, double yPos);

const unsigned int WIDTH = 800;
const unsigned int HEIGHT = 600;

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "ShaderOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glfwSetCursorPosCallback(window, cursorPosCallback);

    /// Wierzchołki dla standardowego trójkąta
    /*float vertices[] = {
            0.0f, 0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f,
            0.5f, -0.5f, 0.0f
    };*/
    /// Wierzchołki dla 2 trójkątów
    float vertices[] = {
            0.0f, -0.5f, 0.0f,
            -0.5f, -0.5f, 0.0f,
            -0.5f, 0.5f, 0.0f,
            0.0f, 0.5f, 0.0f,
            0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f
    };

    /// Vertex Array Object - zawiera dane o przypisanym do niego VBO.
    unsigned int VAO;
    /// Vertex Buffer Object - Przechowuje dane wierchołków.
    unsigned int VBO;

    /// Generowanie obiektów - każdy obiekt w openGl-u opisywany jest unsigned intem a funkcje typu glGen* przypisują odpowiednią liczbę
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    /// Bindowanie - Określa że będziemy używać od teraz stworzonych wcześniej obiektów
    /// VBO jest bufferem więc używa funkcji działających na dowolnych bufferach. Musimy więc podać jakiego typu jest ten buffer.
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);


    /** Przekazujemy dane do VBO:
     * glBufferData(target, size, data, usage)
     * target : Funkcja wpisuje dane do dowolnego buffera więc musimy określić jakiego typu jest nasz
     * size : no rozmiar no
     * data : wskaźnik na tablicę
     * usage : GL_STATIC_DRAW - dane się nie zmieniają
     *         GL_DYNAMIC_DRAW - dane mogą się zmieniać
     *         GL_STREAM_DRAW - dane zmieniają się co chwile
     */
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


    /** Opisujemy jak podzielić nasze dane:
     * tej funkcji nie trzeba rozumieć od razu bo jeszcze będziemy jej używać w ciekawszych warunkach
     * glVertexAttribPointer(index, size, type, normalized, stride, offset)
     * index - określa, który atrybut opisujemy oraz gdzie wyląduje w vertex shaderze
     * size - określa ile danych z tablicy składa się na ten parametr (dla jednego wierchołka)
     * type - typ danych jako GLowy enum
     * normalized - Czy normalizować dane - if(x > 1.0) x = 1.0;
     * stride - odstęp pomiędzy kolejnymi wierzchołkami
     * offset - pierwsze wystąpienie danego atrybutu
     */
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, 3*sizeof(float), (void*)0);
    /// Po opisaniu atrybutu w ten sposób trzeba jeszcze dodać, że chcemy go używać.
    glEnableVertexAttribArray(0);

    /// Unbindujemy VBO i VAO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    /// Generowanie shadera
    unsigned int shader = LoadShader("shaders/shader.vert", "shaders/shader.frag");

    glClearColor(0.4f, 0.4f, 0.6f, 1.0f);
    while (!glfwWindowShouldClose(window))
    {
        glClear(GL_COLOR_BUFFER_BIT);

        ProcessInput(window);

        /// Po stworzeniu shadera deklarujemy, że chcemy go używać
        glUseProgram(shader);
        /// Bindujemy nasze wierzchołki (nie trzeba bindować VBO - jest przypisany do VAO)
        glBindVertexArray(VAO);
        /** Rysujemy trójkąty
         * glDrawArrays(mode, first, count)
         * mode - tryb - GL_POINTS, GL_LINES, GL_LINE_STRIP, GL_TRIANGLES, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN
         *              możliwe, że są jakieś inne jeszcze ale i tak w praktyce używa się głównie GL_TRIANGLES
         * first - od którego zacząć.
         * count - ile wierzchołków narysować (jeżeli (count mod 3 != 0) to GL_TRIANGLES zignoruje ostatnie (count mod 3) wierzchołki)
         */
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void ProcessInput(GLFWwindow* window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    /// Bonus przydatny przy bawieniu się trójkątami
    /// Wciśnij spację aby rysować tylko kontury figur
    /// GL_FRONT_AND_BACK mówi że zmiana ma być nałożona na obie strony trójkąta
    /// To która strona jest przodem określa się na podstawie tego czy wierzchołki pojawiają się na ekranie
    /// zgodnie z ruchem wskazówek zegara. Chwilowo nie ma to znaczenia ale pewnie kiedyś do tego wrócimy.
    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

}

void cursorPosCallback(GLFWwindow* window, double xPos, double yPos){
    glClearColor(float(xPos / WIDTH), float(yPos / HEIGHT), 0.0f, 1.0f);
}

/// Dzika funkcja - dużo kodu - dziwne błędy podczas kopiowania
unsigned int LoadShader(const char* vertexShaderPath, const char* fragmentShaderPath){

    std::string vertexCode;
    std::string fragmentCode;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;

    ///mówimy że chcemy łapać wyjątki - w sumie to nie część openGLa ale komentuję bo mniej znana strona c++sa
    vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        /// otwieramy pliki
        vShaderFile.open(vertexShaderPath);
        fShaderFile.open(fragmentShaderPath);
        std::stringstream vShaderStream, fShaderStream;

        /// czytamy z plików do stringstreamów
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();

        /// zamykamy pliki
        vShaderFile.close();
        fShaderFile.close();

        /// wyciągamy dane z stringstreama do zwykłego stringa
        vertexCode   = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch(std::ifstream::failure e)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }
    /// Kod z plików powinien być podany jako string w stylu C
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();

    /// Tworzymy podprogramy używane w shaderze
    unsigned int vertex, fragment;
    int success;
    char infoLog[512];

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, nullptr);
    /// kompilujemy
    glCompileShader(vertex);

    /// wyłapujemy błędy kompilacji
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    };

    /// teraz to samo dla fragment shadera
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, nullptr);

    /// ---------------------------------------------------------------------------------------------------------
    /// -----------------------TEN FRAGMENT MAGICZNIE SIE NIE SKOPIOWAŁ------------------------------------------
    /// -----------------------ale jak się okazuje czasami działa bez tego i tak---------------------------------
    /// -----------------------Nie zakładajcie że zawsze bo to nie jest standardowe zachowanie-------------------
    /// ---------------------------------------------------------------------------------------------------------
    /// fragment shader też trzeba skompilować
    glCompileShader(fragment);

    /// i sprawdzić błędy
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    };
    ///----------------------------------------------------------------------------------------------------------

    /// teraz łączymy podprogramy w jeden porządny shader program
    unsigned int ID = glCreateProgram();
    glAttachShader(ID, vertex);
    glAttachShader(ID, fragment);
    glLinkProgram(ID);

    /// i wyłapujemy błędy linkowania
    glGetProgramiv(ID, GL_LINK_STATUS, &success);
    if(!success)
    {
        glGetProgramInfoLog(ID, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    /// składowe można bezpiecznie usunąć, żeby nie śmiecić
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    /// zwracamy identyfikator shadera
    return ID;
}